package com.example.rent.sda_049_pb_notifications;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowNotificationActivity extends AppCompatActivity {

    @BindView(R.id.text_view_notification_data)
    protected TextView textViewNotificationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notification);
        ButterKnife.bind(this);

        long notificationData = getIntent().getLongExtra(MainActivity.INTENT_KEY, -1);
        Date date = new Date(notificationData);
        textViewNotificationData.setText(date.toString());


    }
}
